//
//  ViewController.swift
//  App_Final
//
//  Created by Juan Pa on 7/2/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    //MARK:- Outlets
    
    let datosPicker = ["Pelicula","Actor/Actriz"]
    
    var tipoBusqueda = "Pelicula"
    
    @IBOutlet weak var lblBuscarPor: UILabel!
    
    @IBOutlet weak var pickerBusqueda: UIPickerView!
    
    @IBOutlet weak var lblNombre: UILabel!
    
    @IBOutlet weak var txtFieldNombre: UITextField!
    
    struct Variables {
        
        static var nombreBusqueda = ""
        static var idActors = ""
    }
    
    //MARK::- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-: Actions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datosPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return datosPicker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        tipoBusqueda = datosPicker[row]
    }
    
    @IBAction func buttonBuscar(_ sender: Any) {
        
        if(tipoBusqueda == "Pelicula"){
            
            Variables.nombreBusqueda = txtFieldNombre.text!
            
            performSegue(withIdentifier: "Pelicula", sender: self)
            
        }else{
            
            Variables.nombreBusqueda = txtFieldNombre.text!
            
            performSegue(withIdentifier: "Actor", sender: self)
            
        }
        
    }
    
    
}

